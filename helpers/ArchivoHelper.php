<?php

/**
 * Archivo de clase para ArchivoHelper.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 *
 * @link http://www.ktaris.com/
 */

/**
 * Contiene diversas funciones para ayudar en el manejo de archivos.
 */
class ArchivoHelper
{
    /**
     * Crea directorios de manera recursiva.
     * Función tomada de: http://php.net/manual/es/function.mkdir.php.
     *
     * @param string $directorio lista de directorios que se supone deben existir o ser creados.
     * @param string $base       directorio inicial a partir del cual se crearán directorios.
     * @param int    $mode       modo de lectura-escritura-ejecución para los directorios.
     */
    public static function crearDirectorios($directorio, $base = null, $mode = 0777)
    {
        self::crearDirectorio($base, $mode);
        $directorio = str_replace($base, '', $directorio);
        $dirs = explode(DIRECTORY_SEPARATOR, $directorio);
        $count = count($dirs);
        $directorio = $base;
        for ($i = 0; $i < $count; ++$i) {
            $directorio .= DIRECTORY_SEPARATOR.$dirs[$i];
            $resultado = self::crearDirectorio($directorio, $mode);
            if (!$resultado) {
                return false;
            }
        }
        if (!(substr($directorio, -1) === '/' || substr($directorio, -1) === '\\')) {
            $directorio = $directorio.DIRECTORY_SEPARATOR;
        }

        return $directorio;
    }

    /**
     * Crea un directorio si éste no existe.
     *
     * @param string $directorio Nombre del directorio a verificar o crear.
     *
     * @return bool determina si tuvo éxito la creación del directorio (o true si ya estaba creado).
     */
    public static function crearDirectorio($directorio, $mode = 0777)
    {
        $bandera = true;
        if (!file_exists($directorio) || !is_dir($directorio)) {
            $seCreo = mkdir($directorio);
            $seCambioPermiso = chmod($directorio, $mode);
            $bandera = $seCreo & $seCambioPermiso;
        }

        return $bandera;
    }

    /**
     * Almacena un archivo en la ruta seleccionada.
     *
     * @param {@link CActiveRecord} $model         modelo que almacenará el archivo.
     * @param string                $attribute     nombre del atributo con los datos a almacenar.
     * @param string                $directory     ruta destino para el archivo.
     * @param string                $filename      nombre del archivo, sin extensión
     * @param string                $previousValue valor anterior del archivo, por si ya tenía y no se subió nada.
     * @param string                $baseDir       directorio base, a partir del cual se crean los demás directorios.
     */
    public static function almacenarArchivo($model, $attribute, $directory, $filename = null, $previousValue = null, $baseDir = null)
    {
        $directory = self::crearDirectorios($directory, $baseDir);
        $model->{$attribute} = CUploadedFile::getInstance($model, $attribute);
        //Si hay algo, hubo un archivo o algo.
        if ($model->{$attribute}) {
            if ($filename === null) {
                $filename = $model->{$attribute}->name;
            } else {
                $filename = $filename.'.'.$model->{$attribute}->extensionName;
            }
            $model->{$attribute}->saveAs($directory.$filename);
            $model->{$attribute} = $directory.$filename;
            $model->save();
        }
        //Si está vacío el atributo, no hubo nueva subida, así que tomamos el valor anterior.
        if (empty($model->{$attribute})) {
            $model->{$attribute} = $previousValue;
        }
    }

    /**
     * Convierte una ruta absoluta en una ruta relativa.
     *
     * @param string $rutaArchivo  ruta absoluta del archivo.
     * @param string $baseAbsoluta ruta base absoluta.
     * @param string $baseRelativa ruta base relativa.
     *
     * @return string ruta relativa del archivo.
     */
    public static function urlAbsolutaARelativa($rutaArchivo, $baseAbsoluta, $baseRelativa)
    {
        return str_replace($baseAbsoluta, $baseRelativa, $rutaArchivo);
    }

    /**
     * Determina si una ruta apunta hacia un archivo de imagen.
     *
     * @param string $rutaArchivo ruta absoluta de un archivo.
     *
     * @return bool determina si el archivo es imagen.
     */
    public static function esImagen($rutaArchivo)
    {
        $extensionesPermitidas = [
            'jpeg',
            'jpg',
            'bmp',
            'png',
        ];
        $ext = pathinfo($rutaArchivo, PATHINFO_EXTENSION);

        return in_array($ext, $extensionesPermitidas);
    }

    /**
     * Muestra la imagen y la envuelve en una etiqueta "img" para su muestra en HTML.
     *
     * @param string $ruta    ruta del archivo.
     * @param array  $options opciones HTML.
     *
     * @return string HTML que envuelve a la imagen.
     */
    public static function mostrarImagen($ruta, $alt = '', $options = null)
    {
        return CHtml::image($ruta, $alt, $options);
    }

    /**
     * Muestra el archivo y lo envuelve en una etiqueta "a" para su muestra en HTML.
     *
     * @param string $ruta    ruta del archivo.
     * @param array  $options opciones HTML.
     *
     * @return string HTML que envuelve a la imagen.
     */
    public static function mostrarArchivo($ruta, $link = null, $options = null)
    {
        return CHtml::link($link, $ruta, $options);
    }

    /**
     * Muestra una imagen o un vínculo al archivo.
     *
     * @param array $options opciones para mostrar la imagen
     *
     * @return string HTML para mostrar el archivo o imagen.
     */
    public static function mostrarImagenOArchivo($options)
    {
        $options = self::inicializarOpcionesParaMostrarArchivo($options);

        //Revisamos si tenemos una ruta.
        if (empty($options['rutaArchivo'])) {
            return $options['sinArchivo'];
        }
        //Obtenemos la URL.
        $url = self::urlAbsolutaARelativa($options['rutaArchivo'], $options['rutaAbsoluta'], $options['rutaRelativa']);

        //Determinamos si es imagen u otro archivo.
        if (self::esImagen($url)) {
            return self::mostrarImagen($url, $options['imgOptions']['alt'], $options['imgOptions']['htmlOptions']);
        } else {
            return self::mostrarArchivo($url, $options['fileOptions']['texto'], $options['fileOptions']['htmlOptions']);
        }
    }

    /**
     * Inicializa los datos del arreglo para mostrar archivo o imagen.
     *
     * @param array $options arreglo de opciones.
     *
     * @return array arreglo de opciones con valores predeterminados.
     */
    public static function inicializarOpcionesParaMostrarArchivo($options)
    {
        //Propiedades generales.
        if (empty($options['sinArchivo'])) {
            $options['sinArchivo'] = 'SIN ADJUNTO';
        }
        //Propiedades de las imágenes.
        if (empty($options['imgOptions'])) {
            $options['imgOptions'] = [];
        }
        if (empty($options['imgOptions']['alt'])) {
            $options['imgOptions']['alt'] = '';
        }
        if (empty($options['imgOptions']['htmlOptions'])) {
            $options['imgOptions']['htmlOptions'] = [];
        }
        //Propiedades de los archivos.
        if (empty($options['fileOptions'])) {
            $options['fileOptions'] = [];
        }
        if (empty($options['fileOptions']['texto'])) {
            $options['fileOptions']['texto'] = '<i class="glyphicon glyphicon-file"></i> Archivo';
        }
        if (empty($options['fileOptions']['htmlOptions'])) {
            $options['fileOptions']['htmlOptions'] = [];
        }

        return $options;
    }
}
