<?php
/**
 * Archivo de clase para ModelHelper.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 * @link http://www.ktaris.com/
 */

/**
 * Contiene diversas funciones para ayudar en el manejo de modelos.
 */
class ModelHelper
{
	/**
	 * Constantes para la moneda.
	 */
	const MONEDA_USD = 'Dólar';
	const MONEDA_MXN = 'Peso mexicano';

	/**
	 * Regresa un arreglo con los tipos de moneda permitidos.
	 * @return array moneda.
	 */
	public static function monedas(){
		return [
			ModelHelper::MONEDA_MXN => ModelHelper::MONEDA_MXN,
			ModelHelper::MONEDA_USD => ModelHelper::MONEDA_USD,
		];
	}

	/**
	 * Regresa el id de la empresa si ninguno fue provisto.
	 * @param  integer $empresa_id id de la empresa.
	 * @return integer             id de la empresa (incluso si no fue provisto).
	 */
	public static function obtenerEmpresaId($empresa_id)
	{
		if ($empresa_id === null){
			$empresa_id = Yii::app()->user->empresa_id;
		}
		return $empresa_id;
	}

	/**
	 * Regresa una lista de modelos $clase para una empresa en particular.
	 * @param  string  $clase         clase a ser instanciada.
	 * @param  string  $campoAMostrar nombre del campo a mostrar como información del dropdown.
	 * @param  integer $empresa_id    id de la empresa.
	 * @return array   arreglo de modelos.
	 */
	public static function listaPorEmpresaDropdown($clase, $campoAMostrar, $empresa_id = null)
	{
		$empresa_id = ModelHelper::obtenerEmpresaId($empresa_id);
		$criteria = new CDbCriteria;
		$criteria->compare('empresa_id', $empresa_id);
		$models = $clase::model()->findAll($criteria);
		return CHtml::listData($models, 'id', $campoAMostrar);
	}

	/**
	 * Determina si se ha producido un cambio de estado en base al valor actual y el anterior.
	 * @param  integer $valorActual   valor actual de la bandera.
	 * @param  integer $valorAnterior valor anterior de la bandera.
	 * @return boolean                bandera que determina si son diferentes valores.
	 */
	public static function detectarCambioDeEstado($valorActual, $valorAnterior)
	{
		$cambio = $valorActual - $valorAnterior;
		return ($cambio !== 0);
	}

	/**
	 * Determina si se ha producido un cambio de estado positivo (0 a 1) en base al valor actual y el anterior.
	 * @param  integer $valorActual   valor actual de la bandera.
	 * @param  integer $valorAnterior valor anterior de la bandera.
	 * @return boolean                bandera que determina si son diferentes valores.
	 */
	public static function detectarCambioPositivoDeEstado($valorActual, $valorAnterior)
	{
		$cambio = $valorActual - $valorAnterior;
		return ($cambio > 0);
	}
}