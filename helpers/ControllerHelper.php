<?php
/**
 * Archivo de clase para ControllerHelper.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 * @link http://www.ktaris.com/
 */

/**
 * Contiene diversas funciones para ayudar en funciones comunes para controladores.
 */
class ControllerHelper{
	/**
	 * Carga un modelo de $className con la id $id.
	 * @param  string  $className        clase del objeto.
	 * @param  integer $id               id del modelo.
	 * @param string   $mensajeExcepcion mensaje que se muestra en caso de que el modelo no se encuentre.
	 * @return {@link CActiveRecord}     regresa el modelo $className con la $id.
	 */
	public static function loadModel($className, $id, $mensajeExcepcion = null){
		$model = $className::model()->findByPk($id);
		if ($model === null){
			if ($mensajeExcepcion === null){
				$mensajeExcepcion = 'La página solicitada no existe.';
			}
			throw new CHttpException(404, $mensajeExcepcion);
		}
		return $model;
	}

	/**
	 * Realiza la validación mediante AJAX de un modelo, con los datos del formulario.
	 * @param  mixed  $model    modelo a ser validado.
	 * @param  string $formName nombre del formulario del que se extraerán los datos.
	 */
	public static function performAjaxValidation($model, $formName){
		if(isset($_POST['ajax']) && $_POST['ajax'] === $formName){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}