# Saidin


*Saidin* es una extensión para Yii 1 desarrollada en Ktaris, para lidiar con las tareas repetitivas que se hacen de proyecto en proyecto.

Estas tareas o funciones incluyen:

* Generador de cógido con el estándar de Ktaris.
* Nuevos widgets.
* Clases de ayuda para varias tareas comunes en controladores y modelos.
* Componentes base con nuevas propiedades.
