<?php
/**
 * Configuración de mensajes dentro de la extensión encontrado en:
 *     http://stackoverflow.com/questions/22053021/steps-for-making-yiit-work-with-extensions-in-yii
 *     http://www.yiiframework.com/forum/index.php/topic/695-how-to-use-cphpmessagesource/page__p__3746#entry3746
 * Y se reduce a:
 *     'messages'=>array(
 *         'class'=>'CPhpMessageSource',
 *         'extensionPaths'=>array(
 *             'urdimbre' => 'ext.urdimbre.messages',
 *         ),
 *     ),
 */
return array(
    'Add image' => 'Agregar imagen',
    'Add images' => 'Agregar imágenes',
    'Add' => 'Agregar',
    'Advanced Search' => 'Buscar...',
    'are required.' => 'son requeridos.', //Los campos marcados con * son requeridos.
    'Create' => 'Crear',
    'Delete' => 'Eliminar',
    'Fields with' => 'Los campos marcados con', //Los campos marcados con * son requeridos.
    'Invalid request. Please do not repeat this request again.' => 'Petición inválida. Por favor, no intente esto nuevamente.',
    'Manage' => 'Administrar',
    'Save' => 'Guardar',
    'Search' => 'Buscar',
    'Update' => 'Actualizar',
    'View' => 'Ver',
    'Upload image for' => 'Subir imagen para',
    'Are you sure you want to delete this item?' => '¿Está seguro de querer eliminar este artículo?',
);
?>