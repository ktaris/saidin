<?php

class SaidinMenuCuadrado extends CWidget{
    public $_assetsUrl;

    public $elementos = array();
    public $elementosPorFila = 4;
    public $directorioBase = '';

    public function init(){
        Yii::app()->clientScript->registerCssFile($this->assetsUrl . "/saidin.css");
        if(!isset($this->directorioBase) || !$this->directorioBase)
            $this->directorioBase = Yii::app()->baseUrl . '/images/';
    }

    public function run(){
        if (is_array($this->elementos)){
            $i = 0;
            $elementoFinal = end($this->elementos);
            foreach($this->elementos as $e){
                //Estableciendo valores predeterminados.
                $e['url'] = (isset($e['url'])) ? $e['url'] : '#';
                $e['visible'] = (isset($e['visible'])) ? $e['visible'] : true;

                //Iniciando ciclo para visualización.
                if ($e['visible'] == true){
                    $this->render('SaidinMenu', array(
                        'elemento' => $e,
                        'index' => $i,
                    ));
                    $i++;
                }

                //En caso de que sea el último elemento,
                if ($e == $elementoFinal){
                    //Verificamos si debemos cerrar el div de row.
                    //Como ya se incrementó en uno, si estaba en la posición final antes,
                    //ahora ya está en la inicial de nuevo (módulo es cero).
                    //Por lo tanto, si con el incremento no se comienza una nueva línea,
                    //es que el div.row no ha sido cerrado.
                    if (($i % $this->elementosPorFila) != 0){
                        echo '</div>';
                    }
                    //Hacemos un clearfix y dejamos un espacio.
                    echo '<div class="clearfix"></div><br />';
                }
            }
        }
    }

    /**
     * Regresa la clase del elemento en base a los elementos por fila.
     * @return string Clase del elemento (respecto al tamaño).
     */
    public function getClase_elemento(){
        switch ($this->elementosPorFila){
            case 2: $clase = 'col-sm-6'; break;
            case 4: $clase = 'col-sm-3'; break;
            case 6: $clase = 'col-sm-2'; break;
            default: $clase = 'col-sm-4'; break; //3 es predeterminado
        }
        return $clase;
    }

    /**
     * Returns the URL to the published assets folder.
     * @return string an absolute URL to the published asset
     */
    public function getAssetsUrl() {

        if (isset($this->_assetsUrl)) {
            return $this->_assetsUrl;
        } else {
            return $this->_assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('saidin.assets'));
        }
    }
}
?>