<?php

Yii::import('zii.widgets.CMenu');

class SaidinAcciones extends CMenu
{
    public $itemContext = 'default';
    public $itemSize = 'default';
    public $menuAlignment = 'right';

    public $activateItemsOuter = true;

    public function run()
    {
        $this->renderMenu($this->items);
    }

    protected function renderMenuRecursive($items)
    {
        echo '<div class="form-actions" align="'.$this->menuAlignment.'">';
        //Por cada elemento del menú,
        foreach ($items as $item) {
            if ($this->menuAlignment == 'right') {
                echo '&nbsp; &nbsp;';
            }
            $context = (isset($item['context'])) ? $item['context'] : $this->itemContext;
            $size = (isset($item['size'])) ? $item['size'] : $this->itemSize;
            $linkOptions = (isset($item['linkOptions'])) ? $item['linkOptions'] : [];
            $htmlOptions = (isset($item['htmlOptions'])) ? $item['htmlOptions'] : [];
            if (!empty($linkOptions)) {
                if (isset($linkOptions['confirm'])) {
                    $htmlOptions['confirm'] = $linkOptions['confirm'];
                }
                if (isset($linkOptions['submit'])) {
                    $htmlOptions['submit'] = $linkOptions['submit'];
                }
            }
            Yii::app()->controller->widget('booster.widgets.TbButton', array(
                'buttonType' => 'link',
                'context' => $context,
                'label' => $item['label'],
                'url' => $item['url'],
                'size' => $size,
                'htmlOptions' => $htmlOptions,
            ));
            if ($this->menuAlignment == 'left') {
                echo '&nbsp; &nbsp;';
            }
        }
        echo '</div>';
    }
}
