<?php
/**
 * Muestra un menú (un recuadro).
 */
?>
<?php if (($index % $this->elementosPorFila) == 0) { echo '<div class="row">';} ?>
<div class="<?php echo $this->clase_elemento; ?>">
	<a href="<?php echo $elemento['url']; ?>">
	    <div class="text-center center-block">
	    	<h3 class=""><?php echo $elemento['titulo']; ?></h3>
	        <img src="<?php echo $this->directorioBase . $elemento['imagen']; ?>" class="center-block desaturate" />
	    </div>
	</a>
</div>
<?php if (($index % $this->elementosPorFila) == ($this->elementosPorFila - 1)) {
	echo '</div>';
	echo '<br />';
} ?>