<?php
/**
 * Archivo de clase para SaidinController.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 * @link http://www.ktaris.com/
 */

/**
 * Controlador utilizado como base para los controladores creados en los proyectos de Ktaris.
 */
class SaidinController extends CController{
	/**
	 * @var Layout principal a ser utilizado.
	 */
	public $layout = '//layouts/column1';
	/**
	 * @var array Menú contextual de opciones, ligado a {@link CMenu::items}.
	 */
	public $menu = [];
	/**
	 * @var array Migajas asociadas a la página actual, relacionadas con {@link CBreadcrumbs::links}.
	 */
	public $breadcrumbs = [];
	/**
	 * Determina el título a ser mostrado en la vista.
	 */
	public $viewTitle = null;
	/**
	 * @var boolean Bandera que determina si las opciones del menú se deben mostrar o no.
	 */
	public $showMenuOptions = true;
	/**
	 * @var boolean Bandera que determina si las migajas se deben mostrar o no.
	 */
	public $showBreadcrumbs = true;
	/**
	 * @var boolean Bandera que determina si el título de la página se debe mostrar o no.
	 */
	public $showViewTitle = true;

	/**
	 * Establece el título de la vista y el título de la página.
	 * @param string $titulo    título a ser asignado a la vista.
	 * @param string $separador separador entre nombre de la aplicación y título de la vista.
	 */
	public function titulo($titulo, $separador = '|'){
		$this->viewTitle($titulo);
		$this->pageTitle($titulo, $separador);
	}

	/**
	 * Establece el título de la vista.
	 * @param string $titulo    título a ser asignado a la vista.
	 */
	public function viewTitle($titulo){
		$this->viewTitle = $titulo;
	}

	/**
	 * Establece el título de la página.
	 * @param string $titulo    título a ser asignado a la página.
	 * @param string $separador separador entre nombre de la aplicación y título de la vista.
	 */
	public function pageTitle($titulo, $separador = '|'){
		$tituloLimpio = $this->cleanPageTitle($titulo);
		$this->pageTitle = $tituloLimpio . ' ' . $separador . ' ' . Yii::app()->name;
	}

	/**
	 * Elimina las etiquetas de HTML del título provisto.
	 * @param string $separador Texto utilizado de separador entre texto y nombre de la aplicación.
	 * @return string Título de la página.
	 */
	protected function cleanPageTitle($titulo){
		$titulo = str_replace(['<em>', '</em>'], '"', $titulo);
		$titulo = str_replace('<small>', '(', $titulo);
		$titulo = str_replace('</small>', ')', $titulo);
		$titulo = strip_tags($titulo);
		return $titulo;
	}
}