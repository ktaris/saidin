<?php

/**
 *## Clase de desarrollo para Ktaris, para el framework Yii 1.*.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 * @copyright Copyright &copy; Carlos Ramos 2014
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @version 1.0.0
 */

/**
 *## Componente de la aplicación Ktaris.
 *
 * Componente principal de la aplicación. Para su configuración, con las traducciones, widgets, y todo lo demás.
 * se debe utilizar lo siguiente en la configuración principal:
 * PRECARGAR LA APLICACIÓN PARA SU USO:
 *     'preload'=>array('saidin'),
 * PARA UTILIZAR EL GENERADOR DE GII:
 *     'generatorPaths' => array(
 *	       'ext.saidin.gii',
 *	   ),
 * PARA UTILIZAR WIDGETS Y CUANTA MADRE MÁS:
 *	   'saidin'=>array(
 *	       'class'=>'ext.saidin.components.saidin'
 *	   ),
 * PARA CONFIGURAR TRADUCCIONES:
 *	   'messages'=>array(
 *	       'class'=>'CPhpMessageSource',
 *	       'extensionPaths'=>array(
 *	           'saidin' => 'ext.saidin.messages',
 *	       ),
 *	   ),
 */
class Saidin extends CApplicationComponent{
	public function init() {
		//Establece el alias de la aplicación si no está definido.
		$this->setRootAliasIfUndefined();
	}

	/**
	 * Establece el alias de 'saidin' si no está definido.
	 */
	protected function setRootAliasIfUndefined() {
		if (Yii::getPathOfAlias('saidin') === false) {
			Yii::setPathOfAlias('saidin', realpath(dirname(__FILE__) . '/..'));
		}
	}

	/**
	 * Regresa la URL del logo en base a si el tema de Bootstrap es claro u oscuro.
	 *
	 * @param string $tema Nombre del tema a ser utilizado en la aplicación.
	 * @return string URL del logo, ya sea claro u oscuro.
	 */
	public function logo($tema){
		$imagen = 'ktaris.png';
		$path = Yii::app()->theme->baseUrl . '/images/';
		$temasReversa = [
			'cyborg',
			'darkly',
			'slate',
			'superhero',
		];
		if (in_array($tema, $temasReversa)){
			$imagen = 'ktaris_blanco.png';
		}

		return $path . $imagen;
	}

	/**
	 * Función para imprimir alertas, diseñada para trabajar con YiiBooster 4.0.x.
	 */
	public function TbAlertas(){
		Yii::app()->controller->widget('booster.widgets.TbAlert', array(
			'fade' => true,
			'closeText' => '&times;', // false equals no close link
			'events' => array(),
			'htmlOptions' => array(),
			'userComponentId' => 'user',
		));
	}

	/**
	 * Función para generar alertas.
	 * Es básicamente la misma función que setFlash, pero antepone un texto en negritas relativo a la clase del error.
	 *
	 * @param string $clase Clase o color que tendrá la cajita para el error.
	 * @param string $alerta Error o notificación que se genera.
	 */
	public function setFlash($clase, $alerta){
		$negritas = '<strong>¡';
		switch ($clase){
			//Clases con negritas.
			case 'success':   $texto = 'Éxito'; break;
			case 'info':      $texto = 'Aviso'; break;
			case 'warning':   $texto = 'Advertencia'; break;
			case 'error':
			case 'danger':    $texto = 'Error'; break;
			//Clases sin negritas.
			case 'warning-n': $texto = ''; $clase = 'warning'; break;
			//Default.
			default:          $texto = ''; $clase = 'info'; break;
		}
		$negritas .= $texto . '!</strong> ';
		if (!$texto) { $negritas = ''; }
		$alerta = $negritas . $alerta;
		Yii::app()->user->setFlash($clase, $alerta);
	}
}

?>