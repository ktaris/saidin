<?php
/**
 * Archivo de clase para SaidinActiveRecord.
 *
 * @author Carlos Ramos <carlos@ktaris.com>
 * @link http://www.ktaris.com/
 */

/**
 * Contiene la base para los registros con campos de auditoría para saber quién creó, quién actualizó,
 * y cuándo se realizó tal acción.
 */
abstract class SaidinActiveRecord extends CActiveRecord{
	/**
	 * Sobreescritura de la función 'beforeSave' para actualizar la id
	 * de creación o actualización.
	 */
	protected function beforeSave(){
		$id = (Yii::app()->user !== null) ? Yii::app()->user->id : 1;
		if ($this->isNewRecord) { $this->creacion_usuario = $id; }
		$this->actualizacion_usuario = $id;
		return parent::beforeSave();
	}

	/**
	 * Comportamiento para almacenar la fecha de creación y última fecha
	 * de actualización.
	 */
	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'creacion_fecha',
				'updateAttribute' => 'actualizacion_fecha',
				'setUpdateOnCreate' => true,
			),
		);
	}
}
