<?php
/**
 *## Archivo para la clase SaidinGenerator.
 *
 * @author Carlos Ramos <carlos@ramoscarlos.com>
 * @copyright Copyright &copy; Carlos Ramos 2014
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 *
 * Adaptada de la plantilla de YiiBooster, cuyo autor es:
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

Yii::import('gii.generators.crud.CrudGenerator');

/**
 *## Class SaidinGenerator
 *
 * @package saidin.gii
 */
class SaidinGenerator extends CrudGenerator
{
	public $codeModel = 'saidin.gii.saidin.SaidinCode';
}
