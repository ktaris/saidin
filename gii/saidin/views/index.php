<?php
/**
 * @var CModel $model
 */
$class = get_class($model);
Yii::app()->clientScript->registerScript(
	'gii.crud', "
	$('#{$class}_controller').change(function(){
		$(this).data('changed',$(this).val()!='');
	});
	$('#{$class}_model').bind('keyup change', function(){
		var controller=$('#{$class}_controller');
		if(!controller.data('changed')) {
			var id=new String($(this).val().match(/\\w*$/));
			if(id.length>0)
				id=id.substring(0,1).toLowerCase()+id.substring(1);
			controller.val(id);
		}
	});
");
?>

<h1>Generador de Bootstrap 3 para proyectos de Ktaris</h1>


<p>Genera el controlador y las vistas asociadas para el CRUD.</p>


<?php
/** @var CCodeForm $form */
$form = $this->beginWidget('CCodeForm', array('model' => $model)); ?>

<div class="row">
	<?php echo $form->labelEx($model, 'model'); ?>
	<?php echo $form->textField($model, 'model', array('size' => 65)); ?>
	<?php echo $form->error($model, 'model'); ?>
	<div class="tooltip"></div>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'controller'); ?>
	<?php echo $form->textField($model, 'controller', array('size' => 65)); ?>
	<?php echo $form->error($model, 'controller'); ?>
	<div class="tooltip"></div>
</div>

<div class="row sticky">
	<?php echo $form->labelEx($model, '_layout'); ?>
	<?php echo $form->textField($model, '_layout', array('size' => 65)); ?>
	<?php echo $form->error($model, '_layout'); ?>
	<div class="tooltip"></div>
</div>

<div class="row sticky">
	<?php echo $form->labelEx($model, 'baseControllerClass'); ?>
	<?php echo $form->textField($model, 'baseControllerClass', array('size' => 65)); ?>
	<?php echo $form->error($model, 'baseControllerClass'); ?>
	<div class="tooltip"></div>
</div>

<?php $this->endWidget(); ?>
