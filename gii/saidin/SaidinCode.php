<?php
/**
 *## Archivo para la clase SaidinCode.
 *
 * @author Carlos Ramos <carlos@ramoscarlos.com>
 * @copyright Copyright &copy; Carlos Ramos 2014
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 *
 * Adaptada de la plantilla de YiiBooster, cuyo autor es:
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

Yii::import('gii.generators.crud.CrudCode');

/**
 *## Clase SaidinCode
 *
 * @package saidin.gii
 */
class SaidinCode extends CrudCode{
	public $_layout = '//layouts/column1';

	public function generateActiveGroup($modelClass, $column) {

		if ($column->type === 'boolean') {
			return "\$form->checkBoxGroup(\$model,'{$column->name}')";
		} else if (stripos($column->dbType, 'text') !== false) {
			return "\$form->textAreaGroup(\$model,'{$column->name}', ['widgetOptions'=>['htmlOptions'=>['rows'=>6, 'cols'=>50, 'class'=>'span8']]])";
		} else {
			if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
				$inputField = 'passwordFieldGroup';
			} else {
				$inputField = 'textFieldGroup';
			}

			if ($column->type !== 'string' || $column->size === null) {
				if($column->dbType == 'date') {
					return $this->crearCampoFecha($modelClass, $column);
				} else {
					return $this->crearCampoTexto($modelClass, $column, $inputField);
				}
			} else {
				if (strpos ( $column->dbType, 'enum(' ) !== false) {
					$temp = $column->dbType;
					$temp = str_replace ( 'enum', 'array', $temp );
					// FIXME: What. The. Seriously, parse the enum declaration from MySQL as an array definition in PHP?!
					eval ( '$options = ' . $temp . ';' );
					$dropdown_options = "array(";
					foreach ( $options as $option ) {
						$dropdown_options .= "\"$option\"=>\"$option\",";
					}
					$dropdown_options .= ")";
					return "\$form->dropDownListGroup(\$model,'{$column->name}', array('widgetOptions'=>array('data'=>{$dropdown_options}, 'htmlOptions'=>array('class'=>'input-large'))))";
				} else {
					return $this->crearCampoDefault($modelClass, $column, $inputField);
				}
			}
		}
	}

	/**
	 * Regla para validar el dato de layout.
	 */
	public function rules(){
		return array_merge(parent::rules(), array(
			array('_layout', 'required'),
		));
	}

	/**
	 * Regresa el valor del layout.
	 *
	 * @return string Layout a ser utilizado.
	 */
	public function getLayout(){
		if (!isset($this->_layout)){
			return '//layouts/column1';
		}else{
			return $this->_layout;
		}
	}

	/**
	 * Crea un campo tipo fecha.
	 */
	protected function crearCampoFecha($modelClass, $column){
		$campo = "\$form->datePickerGroup(\$model, '{$column->name}', [\n".
			"\t'widgetOptions' => [\n".
				"\t\t'htmlOptions' => [\n".
					"\t\t\t'class' => '',\n".
				"\t\t],\n".
			"\t],\n".
			"\t'prepend'=>'<i class=\"glyphicon glyphicon-calendar\"></i>',\n".
		"])";
		return $campo;
	}

	protected function crearCampoTexto($modelClass, $column, $inputField){
		$campo = "\$form->{$inputField}(\$model, '{$column->name}', [\n".
			"\t'widgetOptions' => [\n".
				"\t\t'htmlOptions' => [\n".
					"\t\t\t'class'=>'',\n".
				"\t\t]\n".
			"\t]\n".
		"])";
		return $campo;
	}

	protected function crearCampoDefault($modelClass, $column, $inputField){
		$campo = "\$form->{$inputField}(\$model,'{$column->name}', [\n".
			"\t'widgetOptions' => [\n".
				"\t\t'htmlOptions' => [\n".
					"\t\t\t'class' => '',\n".
					"\t\t\t'maxlength' => $column->size,\n".
				"\t\t]\n".
			"\t]\n".
		"])";
		return $campo;
	}
}
