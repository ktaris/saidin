<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->titulo('{$this->modelClass} <em>' . \$model->{$this->tableSchema->primaryKey} . '</em>');" . "\n\n";
echo "\$this->breadcrumbs = [
	'$label' => ['index'],
	\$model->{$nameColumn},
];\n";
?>

$this->menu = [
	array('label'=>Yii::t('saidin.app', 'Update') . ' <?php echo $this->modelClass; ?>', 'url'=>['update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>]),
	array('label'=>Yii::t('saidin.app', 'Delete') . ' <?php echo $this->modelClass; ?>', 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>Yii::t('saidin.app', 'Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('saidin.app', 'Create') . ' <?php echo $this->modelClass; ?>', 'url'=>['create']),
	array('label'=>Yii::t('saidin.app', 'Manage') . ' <?php echo $this->modelClass; ?>', 'url'=>['index']),
];
?>

<?php echo "<?php"; ?> $this->widget('booster.widgets.TbDetailView', [
	'data' => $model,
	'attributes' => [
		<?php foreach ($this->tableSchema->columns as $column) {
			echo "\t\t'" . $column->name . "',\n";
		} ?>
	],
]); ?>
