<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
echo '$this->titulo(\'' . $this->pluralize($this->class2name($this->modelClass)) . '\');' . "\n\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'
);\n";
?>

$this->menu = array(
	['label'=>'Crear nuevo <?php echo $this->modelClass; ?>','url'=>['create']],
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
			data: $(this).serialize()
		});
		return false;
	});
");
?>

<?php echo "<?php echo CHtml::link(Yii::t('saidin.app', 'Advanced Search'),'#',array('class'=>'search-button btn')); ?>"; ?>

<div class="search-form" style="display:none">
	<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<div class="clearfix"></div>

<br />

<?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView', [
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider' => $model->search(),
	'type' => 'condensed striped',
	'filter' => $model,
	'columns' => [
		<?php
		$count = 0;
		foreach ($this->tableSchema->columns as $column) {
			if (++$count == 7) {
				echo "\t\t/*\n";
			}
			echo "\t\t'" . $column->name . "',\n";
		}
		if ($count >= 7) {
			echo "\t\t*/\n";
		}
		?>
		[
			'class'=>'booster.widgets.TbButtonColumn',
		],
	],
]); ?>
