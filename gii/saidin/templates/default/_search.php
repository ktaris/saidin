<?php echo "<?php \$form=\$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
	'type'=>'horizontal',
	'htmlOptions' => [
		'class' => 'col-sm-8',
	],
)); ?>\n"; ?>

<?php foreach ($this->tableSchema->columns as $column): ?>
<?php
$field = $this->generateInputField($this->modelClass, $column);
if (strpos($field, 'password') !== false) {
	continue;
}
?>
<?php echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>
<?php endforeach; ?>

<div class="form-actions">
	<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context'=>'primary',
		'label'=>Yii::t('saidin.app', 'Search'),
		'icon'=>'search',
	)); ?>\n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>