<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass . "\n"; ?>
{
	/**
	 * @return array filtros de acciones.
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Especifica las reglas de control de acceso.
	 * Este método es utilizado por el filtro 'accessControl'.
	 * @return array reglas de control de acceso.
	 */
	public function accessRules()
	{
		return [
			['allow', 'actions' => [
				'index',
				'view',
				'create',
				'update',
				'delete',
			], 'users' => ['@']],
			['deny', 'users'=>['*']],
		];
	}

	/**
	 * Lista de todos los modelos.
	 */
	public function actionIndex()
	{
		$model = new <?php echo $this->modelClass; ?>('search');
        $model->unsetAttributes(); //Elimina valores predeterminados.
        if(isset($_GET['<?php echo $this->modelClass; ?>'])){
            $model->attributes = $_GET['<?php echo $this->modelClass; ?>'];
        }

        $this->render('admin', ['model' => $model]);
	}

	/**
	 * Muestra un modelo en particular.
	 * @param integer $id ID del modelo a ser mostrado.
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->render('view', ['model'=>$model]);
	}

	/**
	 * Crea un nuevo modelo.
	 * Si la creación es exitosa, redirecciona a 'index' y muestra un flash.
	 */
	public function actionCreate()
	{
		$model = new <?php echo $this->modelClass; ?>;
		$this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>'])){
			$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()){
				Yii::app()->saidin->setFlash('success', '');
				$this->redirect(['index']);
			}
		}

		$this->render('create', ['model'=>$model]);
	}

	/**
	 * Actualiza un modelo en particular.
	 * Si la actualización es exitosa, se redirigirá a 'index' y mostrará un flash.
	 * @param integer $id ID del modelo a ser actualizado.
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>'])){
			$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()){
				Yii::app()->saidin->setFlash('success', '');
				$this->redirect(['index']);
			}
		}

		$this->render('update', ['model'=>$model]);
	}

	/**
	 * Borra los datos de un modelo en particular.
	 * Si la eliminación es exitosa, se redirigirá a 'index' y mostrará un flash.
	 * @param integer $id ID del modelo a ser eliminado.
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest){
			$this->loadModel($id)->delete();
			if(!isset($_GET['ajax'])){
				Yii::app()->saidin->setFlash('success', '');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
			}
		}else{
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Regresa el modelo de datos basado en la llave primaria de la variable GET.
	 * Si no se encuentra el modelo, se levantará una excepción HTTP.
	 * @param integer ID del modelo a ser cargado.
	 */
	public function loadModel($id)
	{
		return ControllerHelper::loadModel("<?= $this->modelClass ?>", $id);
	}

	/**
	 * Realiza la validación con AJAX.
	 * @param CModel Modelo a ser validado.
	 */
	protected function performAjaxValidation($model)
	{
		ControllerHelper::performAjaxValidation($model, "<?= $this->class2id($this->modelClass) ?>-form");
	}
}
