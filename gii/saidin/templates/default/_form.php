<?php echo "<?php \$form = \$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id' => '" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation' => true,
	'type' => 'horizontal',
	//'focus' => [\$model, ''],
	/*'htmlOptions' => [
		'class' => 'col-sm-offset-2 col-sm-8',
		//'enctype' => 'multipart/form-data' //En caso de transferencia de archivos.
	],//*/
)); ?>\n"; ?>


<p class="help-block"><?php echo Yii::t('saidin.app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('saidin.app', 'are required.'); ?></p>


<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>


<?php
foreach ($this->tableSchema->columns as $column) {
	if ($column->autoIncrement) {
		continue;
	}
	echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n";
}
?>

<div class="form-actions">
	<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => \$model->isNewRecord ? Yii::t('saidin.app', 'Create') : Yii::t('saidin.app', 'Save'),
		'size' => 'large',
		'icon' => 'floppy-disk',
		'block' => true,
	)); ?>\n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
