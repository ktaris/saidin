<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>

<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo '$this->titulo(\'' . $label . '\');' . "\n\n";
echo "\$this->breadcrumbs = array(
	'$label',
);\n";
?>

$this->menu = [
	['label'=>'Create <?php echo $this->modelClass; ?>','url'=>['create']],
	['label'=>'Manage <?php echo $this->modelClass; ?>','url'=>['index']],
];
?>

<?php echo "<?php"; ?> $this->widget('booster.widgets.TbListView', [
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
]); ?>
