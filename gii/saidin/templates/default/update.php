<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->titulo('Actualizar {$this->modelClass} <em>' . \$model->{$this->tableSchema->primaryKey} . '</em>');" . "\n\n";
echo "\$this->breadcrumbs = [
	'$label' => ['index'],
	\$model->{$nameColumn} => ['view','id'=>\$model->{$this->tableSchema->primaryKey}],
	Yii::t('saidin.app', 'Update'),
];\n";
?>

$this->menu = [
	['label'=>Yii::t('saidin.app', 'View') . ' <?= $this->modelClass; ?>','url'=>['view','id'=>$model-><?= $this->tableSchema->primaryKey; ?>]],
	['label'=>Yii::t('saidin.app', 'Create') . ' <?= $this->modelClass; ?>','url'=>['create']],
	['label'=>Yii::t('saidin.app', 'Manage') . ' <?= $this->modelClass; ?>','url'=>['index']],
];
?>


<?php echo "<?= \$this->renderPartial('_form',['model'=>\$model]); ?>"; ?>
