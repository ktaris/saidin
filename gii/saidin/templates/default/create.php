<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo '$this->titulo(\'Crear ' . $this->modelClass . '\');' . "\n\n";
echo "\$this->breadcrumbs = [
	'$label' => ['index'],
	Yii::t('saidin.app', 'Create'),
];\n";
?>

$this->menu = [
	array('label'=>Yii::t('saidin.app', 'List') . ' <?php echo $this->modelClass; ?>', 'url'=>['index']),
];
?>

<?php echo "<?php echo \$this->renderPartial('_form', ['model'=>\$model]); ?>"; ?>
